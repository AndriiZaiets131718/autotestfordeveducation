﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;

namespace PrakticSelenium1.POM
{
    public class Blog
    {
        private IWebDriver _driver;
        public By buttonGoToSecondPageOnBlogPage = By.XPath("/html/body/main/div/div/nav/div/a[1]");
        public By imageGitHubOrGitLabArticle = By.XPath("/html/body/main/div/div/div[2]/div/figure[8]/a/img");
        public By buttonJoin = By.XPath("/html/body/main/section/button");
        public By getJoinFormName = By.XPath("/html/body/div[1]/div/div[3]/form/input[3]");
        public By getJoinFormPhone = By.XPath("/html/body/div[1]/div/div[3]/form/input[4]");
        public By buttonSend = By.XPath("/html/body/div[1]/div/div[3]/form/button");
        public By getClosedItLinkFormEmail = By.XPath("/html/body/main/div/div/div[4]/form/input[1]");
        public By buttonSubscribe = By.XPath("/html/body/main/div/div/div[4]/form/button");

        public Blog(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
         Перейти на 2-ю страницу блога
        */
        public Blog ClickOnButtonGoToSecondPageOnBlogPage()
        {
            _driver.FindElement(buttonGoToSecondPageOnBlogPage).Click();
            return this;
        }
        /*
         Перейти на страницу выбранной статьи
        */
        public Blog ClickOnImageGitHubOrGitLabArticle()
        {
            _driver.FindElement(imageGitHubOrGitLabArticle).Click();
            return this;
        }
        /*
         Кликнуть на кнопку Присоедениться
        */
        public Blog ClickOnButtonJoin()
        {
            _driver.FindElement(buttonJoin).Click();
            return this;
        }
        /*
         Заполнить данными поле Имя в форме Присоедениться
        */
        public Blog EnterToFormInBlogPageName(string name)
        {
            _driver.FindElement(getJoinFormName).SendKeys(name);
            return this;
        }
        /*
         Заполнить данными поле Телефон в форме Присоедениться
        */
        public Blog EnterToFormInBlogPagePhone(string phone)
        {
            _driver.FindElement(getJoinFormPhone).SendKeys(phone);
            return this;
        }
        /*
         Кликнуть по кнопе Отправить и перейти на страницу звонок от менеджера
        */
        public WaitForManagerCallPage ClickOnbuttonSend()
        {
            _driver.FindElement(buttonSend).Click();
            return new WaitForManagerCallPage(_driver);
        }
        /*
         Заполнить данными поле Email в форме Подписаться
        */
        public Blog EnterToFormClosedItLinkFormEmail(string email)
        {
            _driver.FindElement(getClosedItLinkFormEmail).SendKeys(email);
            return this;
        }
        /*
         Кликнуть по кнопке Подписаться на странице Блог
        */
        public CaptchaPage ClickOnButtonSubscribe()
        {
            _driver.FindElement(buttonSubscribe).Click();
            return new CaptchaPage(_driver);
        }

    }

}