﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class DevEducationPageOnAzerbaijani
    {
        private IWebDriver _driver;

        public By mainLabelAz = By.XPath("/html/body/main/div/div[1]/div/h1");

        public DevEducationPageOnAzerbaijani(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindAzerbaijaniText()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(mainLabelAz));
            return _driver.FindElement(mainLabelAz);
        }
        public string GetAzerbaijaniText()
        {
            return FindAzerbaijaniText().Text;
        }


    }
}
