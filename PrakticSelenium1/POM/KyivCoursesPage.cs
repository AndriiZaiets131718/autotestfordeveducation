﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class KyivCoursesPage
    {
        private IWebDriver _driver;
        public By sertificaitButton = By.XPath("/html/body/main/section[3]/div/button");
        //путь к названию поп апа
        public By CheckSertificatePopUpname = By.XPath("/html/body/div[2]/div/div[2]");
        //поле номера сертификата
        public By numberOfSertificate = By.XPath("/html/body/div[2]/div/div[3]/form/input[3]");
        //поле фамилии
        public By secondName = By.XPath("/html/body/div[2]/div/div[3]/form/input[4]");
        //кнопка нажатия на проверку сертификата
        public By checkSertificateButton = By.XPath("/html/body/div[2]/div/div[3]/form/button");
        //текст проверки сертификата
        public By textSertificaitePopUp = By.XPath("/html/body/div[2]/div/div[3]/form/div");
        //текст неудачной проверки сертификата
        public By textSertificatePopUpNotValidSertificate = By.XPath("/html/body/div[2]/div/div[3]/form/div");
        //форма записи на выбранный курс
        public By JavaCoursesInKyivButton = By.XPath("/html/body/main/div/div[2]/div/a[2]");
        



        public KyivCoursesPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        //находим и нажимаем кнопку для получения сертификата
        public KyivCoursesPage ClickOnSertificaitButton()
        {
            _driver.FindElement(sertificaitButton).Click();
            return this;
        }
        //находим название pop up для получения сертификата
        public IWebElement FindSertificatePopUpname()
        {
            return _driver.FindElement(CheckSertificatePopUpname);
        }
        //
        public string GetTextFromSertificatePopUpname()
        {
            return FindSertificatePopUpname().Text;
        }
        public KyivCoursesPage EnterTextToNameField(string name)
        {
            _driver.FindElement(secondName).SendKeys(name);
            return this;
        }
        public KyivCoursesPage EnterNumberOfSertificate(string nameSertificate)
        {
            _driver.FindElement(numberOfSertificate).SendKeys(nameSertificate);
            return this;
        }

        public KyivCoursesPage ClickCheckSertificateButton()
        {
            _driver.FindElement(checkSertificateButton).Click();
            new WebDriverWait(_driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementIsVisible(textSertificaitePopUp));
            return this;
        }
        //сертификат при валидных данных
        public IWebElement FindTextAnswerForPositiveRespond()
        {
            return _driver.FindElement(textSertificaitePopUp);
        }
        
        public string GetTextAnswerForPositiveRespond()
        {
            return FindTextAnswerForPositiveRespond().Text;
        }
        //сертификат при невалидных данных
        public IWebElement FindTextAnswerForNegativeRespond()
        {
            return _driver.FindElement(textSertificatePopUpNotValidSertificate);
        }
        
        public string GetTextAnswerForNegativeRespond()
        {
            return FindTextAnswerForNegativeRespond().Text;
        }

        //форма записи на выбранный курс
        public JavaCoursesKyiv ClickOnJavaCourseButton()
        {
            _driver.FindElement(JavaCoursesInKyivButton).Click();
            return new JavaCoursesKyiv(_driver);
        }



        
    }
}
