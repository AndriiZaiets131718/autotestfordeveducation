﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class DevEducationPageOnENG
    {
        private IWebDriver _driver;

        public By mainLabelENG = By.XPath("/html/body/main/div/div[1]/div/h1");

        public DevEducationPageOnENG(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindEnglishText()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(mainLabelENG));
            return _driver.FindElement(mainLabelENG);
        }
        public string GetEnglishText()
        {
            return FindEnglishText().Text;
        }
    }
}
