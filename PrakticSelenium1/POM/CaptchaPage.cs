﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;

namespace PrakticSelenium1.POM
{
    public class CaptchaPage
    {
        private IWebDriver _driver;
        public By titleFromCaptchaPage = By.XPath("/html/body/div[1]/div/div[1]/form/div[1]");
        public CaptchaPage(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
         Искать титул на страницы каптчи
        */
        public IWebElement TitleOnTheCaptchaPage()
        {
            return _driver.FindElement(titleFromCaptchaPage);
        }
        /*
         Получить текст титула страницы каптчи
        */
        public string GetTextFromTitleOnTheCaptchaPage()
        {
            return TitleOnTheCaptchaPage().Text;
        }
    }
}
