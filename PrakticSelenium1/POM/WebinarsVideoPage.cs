﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;

namespace PrakticSelenium1.POM
{
  public  class WebinarsVideoPage
    {
        private IWebDriver _driver;
        public By videoHowToCombatStress = By.XPath("/html/body/main/div/div[2]/div[1]/div[1]");

        public WebinarsVideoPage(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
        Получить ссылку на видео "Как бороться со стрессом"
       */
        public string FindVideoHowToCombatStress()
        {
            return _driver.FindElement(videoHowToCombatStress).GetAttribute("data-link");
        }
    }
}
