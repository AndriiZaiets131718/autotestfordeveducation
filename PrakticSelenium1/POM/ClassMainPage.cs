﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace PrakticSelenium1.POM
{
    public class ClassMainPage
    {
        private IWebDriver _driver;
        private Actions actions;

        public By mainLabel = By.XPath("/html/body/main/div/div[1]/div/h1");
        public By getGrandFormName = By.XPath("/html/body/main/section[2]/div/form/input[3]");
        public By getGrandFormEmail = By.XPath("/html/body/main/section[2]/div/form/input[5]");
        public By getGrandFormPhone = By.XPath("/html/body/main/section[2]/div/form/input[4]");
        public By getGrandFormCity = By.XPath("/html/body/main/section[2]/div/form/div/select");
        public By getGrandFormDniproCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[4]");
        public By dneprOnMaps = By.XPath("/html/body/main/div/div[4]/div/a[3]");
        public By getGrantButton = By.XPath("/html/body/main/section[2]/div/form/button");
        public By webinarsOnTrainingCenterInDropDown = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/ul/li[2]/a");
        public By contactsButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[4]/a");
       
        //локаторы для кнопки записаться на курс (главная страница)
        public By getMainPage = By.XPath("/html/body");
        public By getSignUpForCourseButton = By.XPath("/html/body/div[3]/button");
        public By getSignUpForCourseFormName = By.XPath("/html/body/div[2]/div/div[3]/form/input[3]");
        public By getSignUpForCourseFormPhone = By.XPath("/html/body/div[2]/div/div[3]/form/input[4]");
        public By getSignUpForCourseFormEmail = By.XPath("/html/body/div[2]/div/div[3]/form/input[5]");
        public By getSignUpForCourseFormCity = By.XPath("/html/body/div[2]/div/div[3]/form/div[1]/select/option[6]");
        public By getSingUpForCourseFormCourse = By.XPath("/html/body/div[2]/div/div[3]/form/div[2]/select/option[4]");
        public By getSignUpForCourseFormButton = By.XPath("/html/body/div[2]/div/div[3]/form/button");
        public By getFindNameForPopUpSingUpForCourses = By.XPath("/html/body/div[2]/div/div[2]");
        //проверка сертификата
        public By getCourseButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[1]/a");
        //локаторы для кнопки смены языка
        public By changeLanguage = By.XPath("/html/body/header/div/div[2]/div");
        public By eng = By.XPath("/html/body/header/div/div[2]/div/ul/li[4]/a");
        public By ukr = By.XPath("/html/body/header/div/div[2]/div/ul/li[1]/a");
        public By az = By.XPath("/html/body/header/div/div[2]/div/ul/li[3]/a");
        //блог
        public By blogOnTrainingCenterInDropDow = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/ul/li[1]/a");

        public ClassMainPage(IWebDriver driver)//конструктор
        {
            this._driver = driver;
            actions = new Actions(driver);
        }



        public IWebElement FindMainLabel()//найти главную надпись на главной странице
        {
            return _driver.FindElement(mainLabel);
        }

        public string GetTextFromMainLabel()//получаем текст из главной надписи
        {
            return FindMainLabel().Text;
        }
        /*
         Вносим данные в поле имя
        */
        public ClassMainPage EnterTextToNameField(string name)
        {
            _driver.FindElement(getGrandFormName).SendKeys(name);
            return this;
        }
        /*
         Вносим данные в поле телефон
        */
        public ClassMainPage EnterTextToPhoneField(string phone)
        {
            _driver.FindElement(getGrandFormPhone).SendKeys(phone);
            return this;
        }
        /*
         Вносим данные в поле е-мейл
        */
        public ClassMainPage EnterTextToEmailField(string email)
        {
            _driver.FindElement(getGrandFormEmail).SendKeys(email);
            return this;
        }
        /*
         Клик по выпадающему меню выбора города
        */
        public ClassMainPage ClickOnCityDropDown()
        {
            _driver.FindElement(getGrandFormCity).Click();
            return this;
        }
        /*
        Выбираем город Днепр
        */
        public ClassMainPage ClickOnDnipro()
        {
            _driver.FindElement(getGrandFormDniproCity).Click();
            return this;
        }
        /*
         Клик по кнопке Получить грант
        */
        public WaitForManagerCallPage ClickOnGrantButton()
        {
            _driver.FindElement(getGrantButton).Click();
            return new WaitForManagerCallPage(_driver);
        }
        /*
         Клик по городу Днепр на картке на главной странице
        */
        public DniproDevCollegeMainPage ClickOnDneprOnTheMap()
        {
            _driver.FindElement(dneprOnMaps).Click();
            return new DniproDevCollegeMainPage(_driver);
        }
        /*
        Переход через выпадающий список из обучающий центр на страницу Вебинары
        */
        public WebinarsPage ClickOnWebinarsOnTrainingCenterInDropDown()

        {
                IWebElement button = _driver.FindElement(webinarsOnTrainingCenterInDropDown);
                actions.MoveToElement(_driver.FindElement(By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/a")));
                actions.Perform();
                button.Click();
                return new WebinarsPage(_driver);
    
        }
        /*
         Переход на стрницу Контакты
        */
        public Contacts ClickOnContactsButton()
        {
            _driver.FindElement(contactsButton).Click();
            return new Contacts(_driver);
        }
        //Записаться на курс (на главной страничке)
        public ClassMainPage FindSignUpForCourseButton()
        {
            _driver.FindElement(getMainPage).SendKeys(Keys.Space);
            return this;
        }
        public ClassMainPage ClickSignUpForCourseButton()
        {
            _driver.FindElement(getSignUpForCourseButton).Click();
            return this;
        }
        public ClassMainPage EnterTextToNameFieldForSignUpForm(string nameSignUpForm)
        {
            _driver.FindElement(getSignUpForCourseFormName).SendKeys(nameSignUpForm);
            return this;
        }
        public ClassMainPage EnterTextToPhoneFieldForSignUpForm(string phoneSignUpForm)
        {
            _driver.FindElement(getSignUpForCourseFormPhone).SendKeys(phoneSignUpForm);
            return this;
        }
        public ClassMainPage EnterTextToEmailFieldForSignUpForm(string emailSignUpForm)
        {
            _driver.FindElement(getSignUpForCourseFormEmail).SendKeys(emailSignUpForm);
            return this;
        }
        public ClassMainPage EnterCityForSignUpForm()
        {
            _driver.FindElement(getSignUpForCourseFormCity).Click();
            return this;
        }
        public ClassMainPage EnterCourseForSignUpForm()
        {
            _driver.FindElement(getSingUpForCourseFormCourse).Click();
            return this;
        }
        public WaitForManagerCallPage ClickOnSendSignUpFormButton()
        {
            _driver.FindElement(getSignUpForCourseFormButton).Click();
            return new WaitForManagerCallPage(_driver);
        }
        //не заполненные поля
        public IWebElement FindNameForPopUpSingUpForCourses()
        {
            return _driver.FindElement(getFindNameForPopUpSingUpForCourses);
        }

        public string GetNameForPopUpSingUpForCourses()//получаем текст из главной надписи
        {
            return FindNameForPopUpSingUpForCourses().Text;
        }
        //проверка сертификата
        public Courses ClickOnCoursesButton()
        {
            _driver.FindElement(getCourseButton).Click();
            return new Courses(_driver);
        }

        //проверка локализации с русского на англ
        public ClassMainPage ClickOnLanguageMenu()
        {
            _driver.FindElement(changeLanguage).Click();
            return this;
        }
        public DevEducationPageOnENG LanguageSelectionENG()
        {
            _driver.FindElement(eng).Click();
            return new DevEducationPageOnENG(_driver);
        }
        //проверка локализации с русского на украинский
        public DevEducationPageOnUkrainian LanguageSelectionUKR()
        {
            _driver.FindElement(ukr).Click();
            return new DevEducationPageOnUkrainian(_driver);
        }
        //проверка локализации с русского на азербайджанский
        public DevEducationPageOnAzerbaijani LanguageSelectionAz()
        {
            _driver.FindElement(az).Click();
            return new DevEducationPageOnAzerbaijani(_driver);
        }

        /*
         Переход через выпадающий список из обучающий центр на страницу Блог
        */
        public Blog ClickOnBlogsOnTrainingCenterInDropDown()

        {
            IWebElement button = _driver.FindElement(blogOnTrainingCenterInDropDow);
            actions.MoveToElement(_driver.FindElement(By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/a")));
            actions.Perform();
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(blogOnTrainingCenterInDropDow));
            button.Click();
            return new Blog(_driver);

        }
    }
}
    

