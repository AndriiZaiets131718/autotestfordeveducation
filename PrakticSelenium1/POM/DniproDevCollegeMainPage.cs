﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace PrakticSelenium1.POM
{
    public class DniproDevCollegeMainPage
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By dniproLocation = By.XPath("/html/body/header/div/div[2]/div[2]/span");
        public By floatingButtonSignUpOnCurse = By.XPath("/html/body/div[4]");
        public By faqLabelDescription = By.XPath("/html/body/main/section[6]/div/span");
        public By getSignUpFormName = By.XPath("/html/body/div[3]/div/div[3]/form/input[3]");
        public By getSignUpFormEmail = By.XPath("/html/body/div[3]/div/div[3]/form/input[5]");
        public By getSignUpFormPhone = By.XPath("/html/body/div[3]/div/div[3]/form/input[4]");
        public By getSignUpFormCity = By.XPath("/html/body/div[3]/div/div[3]/form/div[1]/select");
        public By getSignUpFormDniproCity = By.XPath("/html/body/div[3]/div/div[3]/form/div[1]/select/option[4]");
        public By getSignUpFormCourses = By.XPath("/html/body/div[3]/div/div[3]/form/div[2]/select");
        public By getSignUpFormQaCourses = By.XPath("/html/body/div[3]/div/div[3]/form/div[2]/select/option[7]");
        public By getSignUpButton = By.XPath("/html/body/div[3]/div/div[3]/form/button");
        public DniproDevCollegeMainPage(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
            

        }
        public IWebElement FindDniproLocatin()
        {
            return _driver.FindElement(dniproLocation);
        }
        /*
        Кликнуть по закреплённой кнопке Записаться на курс
       */
        public DniproDevCollegeMainPage ClickFloatingSignButton()
        {
            IWebElement button = _driver.FindElement(floatingButtonSignUpOnCurse);
            _actions.MoveToElement(_driver.FindElement(By.XPath("/html/body/main/section[4]/div/form/input[3]")));
            _actions.Perform();
            button.Click();
            return this;
        }
        /*
        Ввести данные в поле имя
       */
        public DniproDevCollegeMainPage EnterTextToNameFieldOnSignUpForm(string name)
        {
            _driver.FindElement(getSignUpFormName).SendKeys(name);
            return this;
        }
        /*
        Ввести данные в поле телефон
       */
        public DniproDevCollegeMainPage EnterTextToPhoneFieldOnSignUpForm(string phone)
        {
            _driver.FindElement(getSignUpFormPhone).SendKeys(phone);
            return this;
        }
        /*
        Ввести данные в поле е-мейл
       */
        public DniproDevCollegeMainPage EnterTextToEmailFieldOnSignUpForm(string email)
        {
            _driver.FindElement(getSignUpFormEmail).SendKeys(email);
            return this;
        }
        /*
        Кликнуть по выпадающему меню выбора города
       */
        public DniproDevCollegeMainPage ClickOnCityDropDownOnSignUpForm()
        {
            _driver.FindElement(getSignUpFormCity).Click();
            return this;
        }
        /*
        Выбрать город Днепр
      */
        public DniproDevCollegeMainPage ClickOnDniproOnSignUpForm()
        {
            _driver.FindElement(getSignUpFormDniproCity).Click();
            return this;
        }
        /*
       Кликнуть по выпадающему меню выбора курсов
      */
        public DniproDevCollegeMainPage ClickOnCoursesDropDown()
        {
            _driver.FindElement(getSignUpFormCourses).Click();
            return this;
        }
        /*
       Выбрать курсы QA/AT
      */
        public DniproDevCollegeMainPage ClickOnQA()
        {
            _driver.FindElement(getSignUpFormQaCourses).Click();
            return this;
        }
        /*
       Кликнуть по кнопке Отправить
      */
        public WaitForManagerCallPage ClickOnSendButton()
        {
            _driver.FindElement(getSignUpButton).Click();
            return new WaitForManagerCallPage(_driver);
        }

    }
}
