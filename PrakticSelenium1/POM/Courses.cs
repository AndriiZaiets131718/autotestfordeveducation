﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class Courses
    {
        private IWebDriver _driver;

        public By getKyivCourseButton = By.XPath("/html/body/main/div/div[2]/div/div[4]/a[1]");

        public Courses(IWebDriver driver)
        {
            this._driver = driver;
        }
        //найти кнопку курсов в Киеве
        public KyivCoursesPage ClickOnKyivCoursesButton()
        {
            _driver.FindElement(getKyivCourseButton).Click();
            return new KyivCoursesPage(_driver);
        }
    }
}
