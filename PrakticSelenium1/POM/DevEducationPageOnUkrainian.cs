﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class DevEducationPageOnUkrainian
    {
        private IWebDriver _driver;

        public By mainLabelUKR = By.XPath("/html/body/main/div/div[1]/div/h1");

        public DevEducationPageOnUkrainian(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindUkrainianText()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(mainLabelUKR));
            return _driver.FindElement(mainLabelUKR);
        }
        public string GetUkrainianText()
        {
            return FindUkrainianText().Text;
        }

    }
}
