﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class JavaCoursesKyiv
    {
        private IWebDriver _driver;

        public By getJavaCoursesButton = By.XPath("/html/body/main/div/section[1]/div/button");
        public By getJavaCoursesNameField = By.XPath("/html/body/div[3]/div/div[3]/form/input[3]");
        public By getJavaCoursesPhoneField = By.XPath("/html/body/div[3]/div/div[3]/form/input[4]");
        public By getJavaCoursesEmailField = By.XPath("/html/body/div[3]/div/div[3]/form/input[5]");
        public By getJavaCoursesDropDownKyiv = By.XPath("/html/body/div[3]/div/div[3]/form/div[1]/select/option[5]");
        public By getJavaCoursesDropDownKyivCourse = By.XPath("/html/body/div[3]/div/div[3]/form/div[2]/select/option[3]");
        public By getJavaCoursesKyivButtonForSendForm = By.XPath("/html/body/div[3]/div/div[3]/form/button");
        public By getTitlePopUpKyivCourses = By.XPath("/html/body/div[3]/div/div[2]");

        public JavaCoursesKyiv(IWebDriver driver)
        {
            this._driver = driver;
        }

        public JavaCoursesKyiv ClickOnSertificaitButton()
        {
            _driver.FindElement(getJavaCoursesButton).Click();
            return this;
        }
        
        //заполняем форму записи на курс java
        public JavaCoursesKyiv EnterTextToNameFieldJavaCourse(string nameJavaCourse)
        {
            _driver.FindElement(getJavaCoursesNameField).SendKeys(nameJavaCourse);
            return this;
        }
        public JavaCoursesKyiv EnterTextToPhoneFieldJavaCourse(string phoneJavaCourse)
        {
            _driver.FindElement(getJavaCoursesPhoneField).SendKeys(phoneJavaCourse);
            return this;
        }
        public JavaCoursesKyiv EnterTextToEmailFieldJavaCourse(string emailJavaCourse)
        {
            _driver.FindElement(getJavaCoursesEmailField).SendKeys(emailJavaCourse);
            return this;
        }

        public JavaCoursesKyiv ClickOnCityDropDownKyiv()
        {
            _driver.FindElement(getJavaCoursesDropDownKyiv).Click();
            return this;
        }
        public JavaCoursesKyiv ClickOnCoursesDropDownJava()
        {
            _driver.FindElement(getJavaCoursesDropDownKyivCourse).Click();
            return this;
        }
        public WaitForManagerCallPage ClickOnJavaCoursesKyivButtonForSendForm()
        {
            _driver.FindElement(getJavaCoursesKyivButtonForSendForm).Click();
            return new WaitForManagerCallPage(_driver);
        }
        public JavaCoursesKyiv ClickOnJavaCoursesKyivButtonForSendFormNotValid()
        {
            _driver.FindElement(getJavaCoursesKyivButtonForSendForm).Click();
            return this;
        }
        //название Pop up 
        public IWebElement FindPopUpNameForJavaCoursesKyiv()
        {
            return _driver.FindElement(getTitlePopUpKyivCourses);
        }
        public string GetPopUpNameForJavaCoursesKyiv()
        {
            return FindPopUpNameForJavaCoursesKyiv().Text;
        }
    }
}
