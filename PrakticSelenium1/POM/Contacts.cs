﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;
namespace PrakticSelenium1.POM
{
    public class Contacts
    {
        private IWebDriver _driver;
        public By titleOurContacts = By.XPath("/html/body/main/div/div[2]/h1");
        public By tabDnepr = By.XPath("/html/body/main/div/div[2]/div[1]/div[3]");
        public By titleDnepr = By.XPath("/html/body/main/div/div[2]/div[4]/div[1]/div[1]");
        public By adressDnepr = By.XPath("/html/body/main/div/div[2]/div[4]/div[1]/div[2]");
        public By phoneDnepr = By.XPath("/html/body/main/div/div[2]/div[4]/div[1]/div[3]");
        public By emailDnepr = By.XPath("/html/body/main/div/div[2]/div[4]/div[1]/div[4]");
        public By map = By.XPath("/html/body/main/div/div[2]/div[4]/div[2]/embed");
        public By buttonAskQuestion = By.XPath("/html/body/main/section/div/button");
        public By getAskQuestionFormName = By.XPath("/html/body/div[1]/div/div[3]/form/input[3]");
        public By getAskQuestionFormAskYourQuestion = By.XPath("/html/body/div[1]/div/div[3]/form/input[5]");
        public By getAskQuestionFormPhone = By.XPath("/html/body/div[1]/div/div[3]/form/input[4]");
        public By buttonSendQuestion = By.XPath("/html/body/div[1]/div/div[3]/form/button");
        
        


        public Contacts(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
         Найти титул на странице Наши контакты
        */
        public IWebElement FindTitleOurContacts()
        {
            return _driver.FindElement(titleOurContacts);
        }
        /*
         Получить текст из титула Наши контакты
        */
        public string GetTextFromTitleOurContacts()
        {
            return FindTitleOurContacts().Text;
        }
        /*
         Кликнуть по вкладке Днепр
        */
        public Contacts ClickOnTabDnepr()
        {
            _driver.FindElement(tabDnepr).Click();
            return this;
        }
        /*
         Найти титул на странице Днепра
        */
        public IWebElement FindTitleDnepr()
        {
            return _driver.FindElement(titleDnepr);
        }
        /*
         Получить текст из титула на странице Днепра
        */
        public string  GetTextFromTitleDnepr()
        {
            return FindTitleDnepr().Text;
        }
        /*
         Найти адресс в Днепре
        */
        public IWebElement FindAdressDnepr()
        {

            return _driver.FindElement(adressDnepr);
        }
        /*
         Найти телефон в Днепре
        */
        public IWebElement FindPhoneDnepr()
        {
            return _driver.FindElement(phoneDnepr);
        }
        /*
        Найти е-мейл в Днепре
       */
        public IWebElement FindEmailDnepr()
        {
            return _driver.FindElement(emailDnepr);
        }
        /*
        Получить текст адресса 
       */
        public string GetTextFromAdressDnepr()
        {
            return FindAdressDnepr().Text;
        }
        /*
        Получить текст телефона
       */
        public string GetTextFromPhoneDnepr()
        {
            return FindPhoneDnepr().Text;
        }
        /*
        Получить текст е-мейла
       */
        public string GetTextFromEmailDnepr()
        {
            return FindEmailDnepr().Text;
        }
        /*
        Найти карту
       */
        public IWebElement FindMap()
        {
            return _driver.FindElement(map);
        } 
        /*
        Проверка отображения карты Днепра
        */
        public string CheckTheMap()

        {
            return FindMap().GetAttribute("src");
        }
        /*
        Клик по кнопке Задать вопрос
       */
        public Contacts ClickOnButtonAskQuestion()
        {
            _driver.FindElement(buttonAskQuestion).Click();
            return this;
        }
        /*
        Ввести данные в поле имя
       */
        public Contacts EnterTextInFormAskQuestionToNameField(string name)
        {
            _driver.FindElement(getAskQuestionFormName).SendKeys(name);
            return this;
        }
        /*
        Ввести данные в поле телефон
       */
        public Contacts EnterTextInFormAskQuestionToPhoneField(string phone)
        {
            _driver.FindElement(getAskQuestionFormPhone).SendKeys(phone);
            return this;
        }
        /*
        Ввести данные в задать вопрос
       */
        public Contacts EnterTextInFormAskQuestionToAskYourQuestionField(string question)
        {
            _driver.FindElement(getAskQuestionFormAskYourQuestion).SendKeys(question);
            return this;
        }
        /*
        Кликнуть по кнопке отправть вопрос
       */
        public WaitForManagerCallPage ClickOnButtonSendQuestion()
        {
            _driver.FindElement(buttonSendQuestion).Click();
            return new WaitForManagerCallPage(_driver);
        }
        



    }
}
