﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support;

namespace PrakticSelenium1.POM
{
    public class WaitForManagerCallPage
    {
        private IWebDriver _driver;
        public By dontMissCallLabel = By.XPath("/html/body/div/div[2]/div/div/div[2]/div[1]");
        public By descriptionLabel = By.XPath("/html/body/div/div[2]/div/div/div[2]/div[2]");
        

        public WaitForManagerCallPage(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
        Найти титул не пропусти звонок
        */
        public IWebElement FindDontMissCallLabel()
        {
            return _driver.FindElement(dontMissCallLabel);
        }
        /*
        Найти описание титула
        */
        public IWebElement FindDescriptionLabel()
        {
            return _driver.FindElement(descriptionLabel);
        }
        /*
         Получить текст титула не пропусти звонок
        */ 
        public string GetTextFromDontMissCallLabel()
        {
            return FindDontMissCallLabel().Text;
        }
        /*
        Получить текст описания титула
        */
        public string GetTextFromDontDescriptionLabel()
        {
            return FindDescriptionLabel().Text;
        }
    }
}

