﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrakticSelenium1.POM
{
    public class WebinarsPage
    {
        private IWebDriver _driver;
        public By showMoreWebinarsButton = By.XPath("/html/body/main/div/div[3]");
        public By imageWithVideoHowToCombatStress = By.XPath("/html/body/main/div/div[2]/div[2]/figure[13]/a/div/img");
        public By getFormInWebinarsPageName = By.Name("full_name");
        public By getFormInWebinarsPagePhone = By.Name("phone");
        public By buttonSendInWebinarsPage = By.XPath("/html/body/main/section/div/form/button");
        public By checkBoxInWebinarsPage = By.XPath("/html/body/main/section/div/form/div/label/input");
        public WebinarsPage(IWebDriver driver)
        {
            this._driver = driver;
        }
        /*
         Найти кнопку Показать больше вебинаров
        */
        public IWebElement FindbuttonShowMoreWebinars()
        {
            return _driver.FindElement(showMoreWebinarsButton);
        }
        /*
         Кликнуть по кнопке Показать больше вебинаров
        */
        public WebinarsPage ClickOnShowMoreWebinarsButton()
        {
            _driver.FindElement(showMoreWebinarsButton).Click();
            return this;
        }
        /*
         Кликнуть по картинке Как бороться со стрессом
        */
        public WebinarsVideoPage ClickOnImageWithVideoHowToCombatStress()
        {
            _driver.FindElement(imageWithVideoHowToCombatStress).Click();
            return new WebinarsVideoPage(_driver);
        }
        /*
         Вносим данные в поле имя
        */
        public WebinarsPage EnterToFormInWebinarsPageName(string name)
        {
            _driver.FindElement(getFormInWebinarsPageName).SendKeys(name);
            return this;
        }
        /*
         Вносим данные в поле телефон
        */
        public WebinarsPage EnterToFormInWebinarsPagePhone(string phone)
        {
            _driver.FindElement(getFormInWebinarsPagePhone).SendKeys(phone);
            return this;
        }
        /*
         Кликаем проверяем заполненоность чекбокса и кликаем по кнопке отправить
        */
        public WaitForManagerCallPage ClickOnTheButtonSendAfterCheckCheckBox()
        {
            
            if (_driver.FindElements(checkBoxInWebinarsPage).Count>0)
            {
                _driver.FindElement(buttonSendInWebinarsPage).Click();
                
            }
            else
            {
                _driver.FindElement(checkBoxInWebinarsPage).Click();
                _driver.FindElement(buttonSendInWebinarsPage).Click();

            }
            return new WaitForManagerCallPage(_driver);
        }
        

        }
    }

   

