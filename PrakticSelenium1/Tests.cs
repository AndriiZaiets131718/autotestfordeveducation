﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PrakticSelenium1.POM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Interactions;


namespace PrakticSelenium1
{
    [TestFixture]
    [Parallelizable]
    public class Tests
    {
        public IWebDriver driver;
        public Actions actions;
        public ClassMainPage mainPage;
        public WaitForManagerCallPage callPage;
        public DniproDevCollegeMainPage dniproMainPage;
        public WebinarsPage webinars;
        public WebinarsVideoPage webinarVideoPage;
        public Contacts contactsPage;
        public KyivCoursesPage kyivCoursesPage;
        public Courses courses;
        public DevEducationPageOnENG devEducationPageOnENG;
        public DevEducationPageOnUkrainian devEducationPageOnUkrainian;
        public DevEducationPageOnAzerbaijani devEducationPageOnAzerbaijani;
        public JavaCoursesKyiv javaCoursesKyiv;
        public Blog blogPage;
        public CaptchaPage captcha;

        private string _nameGetGrantForm = "Test";
        private string _emailGetGrantForm = "dog@nameTest.com";
        private string _phoneGetGrantForm = "0678880978";

        [OneTimeSetUp]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"D:\Selenium");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
        }
        [SetUp]
        public void OpenMainPage()
        {
            driver.Navigate().GoToUrl("https://deveducation.com/");
            driver.Manage().Window.Maximize();
            mainPage = new ClassMainPage(driver);
        }
        /*
         Проверка,что мы на главной странице
         */
        [Test]
        public void CheckMainLabelTextOnMainPage()
        {
            string mainElementText = mainPage.GetTextFromMainLabel();
            Assert.AreEqual("Международный IT-колледж", mainElementText);

        }

        /*
         Отправка получить грант с главной страницы
         */

        [Test]

        public void FillGetGrantForm()
        {
            callPage = mainPage.EnterTextToNameField("Test").
                   EnterTextToEmailField("Test@i.ua").
                   EnterTextToPhoneField("123213").
                   ClickOnCityDropDown().
                   ClickOnDnipro().
                   ClickOnGrantButton();
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);
        }

        /*
         Переход через карту на страницу Днепра и клика на фиксированную кнопку,заполнить форму и записаться на курс.
        */
        [Test]
        public void FillGetSignUpFormFromDniproCoursesPage()
        {
            dniproMainPage = mainPage.ClickOnDneprOnTheMap();
            callPage = dniproMainPage.ClickFloatingSignButton().
                EnterTextToNameFieldOnSignUpForm("Test").
            EnterTextToEmailFieldOnSignUpForm("Test@i.ua").
            EnterTextToPhoneFieldOnSignUpForm("123213").
            ClickOnCityDropDownOnSignUpForm().
            ClickOnDniproOnSignUpForm().
            ClickOnCoursesDropDown().
            ClickOnQA().
            ClickOnSendButton();
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);
        }

        /*
         Проверка наличия видео на странице вебинары
        */

        [Test]
        public void CheckTheVideoInWebinarsPage()
        {
            webinars = mainPage.ClickOnWebinarsOnTrainingCenterInDropDown();
            webinarVideoPage = webinars.ClickOnShowMoreWebinarsButton().
                ClickOnImageWithVideoHowToCombatStress();
            webinarVideoPage.FindVideoHowToCombatStress();
            string videoHowToCombatStress = webinarVideoPage.FindVideoHowToCombatStress();
            Assert.AreEqual("https://www.youtube.com/watch?v=YqkYcMkWyQY&ab_channel=DevEducation",
                videoHowToCombatStress);
        }
        /*
        Проверка наличия всех данных на вкладке Днепр в Контактах
        */

        [Test]

        public void CheckDneprFromContactsPage()
        {
            contactsPage = mainPage.ClickOnContactsButton();
            string titleOurContacts = contactsPage.GetTextFromTitleOurContacts();
            Assert.AreEqual("Наши контакты", titleOurContacts);
            contactsPage.ClickOnTabDnepr();

            string titleDnepr = contactsPage.GetTextFromTitleDnepr();
            string adressDnepr = contactsPage.GetTextFromAdressDnepr();
            string phoneDnepr = contactsPage.GetTextFromPhoneDnepr();
            string emailDnepr = contactsPage.GetTextFromEmailDnepr();
            string map = contactsPage.CheckTheMap();

            Assert.AreEqual("Днепр", titleDnepr);
            Assert.AreEqual("ул.Симферопольская, 17", adressDnepr);
            Assert.AreEqual("+38 096 861 73 72", phoneDnepr);
            Assert.AreEqual("info@deveducation.com", emailDnepr);
            Assert.AreEqual("https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2646.3539660793294!2d35.067817846243145!3d48.449737664257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbfd2ab9fc1503%3A0x1464293628b13ae!2z0YPQuy4g0KHQuNC80YTQtdGA0L7Qv9C-0LvRjNGB0LrQsNGPLCAxNywg0JTQvdC40L_RgNC-LCDQlNC90LXQv9GA0L7Qv9C10YLRgNC-0LLRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNDkwMDA!5e0!3m2!1sru!2sua!4v1586956191062!5m2!1sru!2sua"
                , map);
        }
        /*
         Проверка отправки формы Задать вопрос со строницы Контакты
        */

        [Test]
        public void FillGetSendQuestionForm()
        {
            contactsPage = mainPage.ClickOnContactsButton();
            callPage = contactsPage.ClickOnButtonAskQuestion().
                EnterTextInFormAskQuestionToNameField("Test Testovi4 Testerovskiy").
                EnterTextInFormAskQuestionToPhoneField("1234567891").
                EnterTextInFormAskQuestionToAskYourQuestionField("Test question send").
                ClickOnButtonSendQuestion();
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);

            /*
            Проверка отправки формы со страницы Вебинары
           */

        }
        [Test]
        public void CheckFormOnWebinarsPage()
        {
            webinars = mainPage.ClickOnWebinarsOnTrainingCenterInDropDown();
            callPage = webinars.EnterToFormInWebinarsPageName("TestNameTestovi4").
                EnterToFormInWebinarsPagePhone("11113333").
                ClickOnTheButtonSendAfterCheckCheckBox();
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);

        }
        //------------------------------------------------------------------------------------------------------------
        //Заполнение валидными данными формы записи на курс (с помощью кнопки "Записаться на курс" на главной странице)
        [Test]
        public void FillGetGrantFormOnMainPage()
        {
            callPage = mainPage.FindSignUpForCourseButton()
           .ClickSignUpForCourseButton()
           .EnterTextToNameFieldForSignUpForm(_nameGetGrantForm)
           .EnterTextToPhoneFieldForSignUpForm(_phoneGetGrantForm)
           .EnterTextToEmailFieldForSignUpForm(_emailGetGrantForm)
           .EnterCityForSignUpForm()
           .EnterCourseForSignUpForm()
           .ClickOnSendSignUpFormButton(); //последний метод переходит на новую сртраничку
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);
        }
        [Test]
        //Проверка возможности отправки пустой формы записи на курс (с помощью кнопки "Записаться на курс" на главной странице)
        public void EmptyGetGrantFormOnMainPage()
        {
            mainPage.FindSignUpForCourseButton()
           .ClickSignUpForCourseButton()
           .EnterTextToNameFieldForSignUpForm("")
           .EnterTextToPhoneFieldForSignUpForm("")
           .EnterTextToEmailFieldForSignUpForm("")
           .ClickOnSendSignUpFormButton();
            string PopUpNameForSignUpOnCoursePage = mainPage.GetNameForPopUpSingUpForCourses();
            Assert.AreEqual("Записаться на курс", PopUpNameForSignUpOnCoursePage);

        }
        [Test]
        //Проверка возможности отправки формы заполненной невалидными значениями (отправка формы "Записаться на курс" с главной странице)
        public void NotValidDataInGrantFormOnMainPage()
        {
            mainPage.FindSignUpForCourseButton()
           .ClickSignUpForCourseButton()
           .EnterTextToNameFieldForSignUpForm(" ")
           .EnterTextToPhoneFieldForSignUpForm("NotPhone")
           .EnterTextToEmailFieldForSignUpForm("mail")
           .EnterCityForSignUpForm()
           .EnterCourseForSignUpForm()
           .ClickOnSendSignUpFormButton();
            string PopUpNameForSignUpOnCoursePage = mainPage.GetNameForPopUpSingUpForCourses();
            Assert.AreEqual("Записаться на курс", PopUpNameForSignUpOnCoursePage);
        }
        [Test] //проверка сертификата при заполнении полей валидными данными
        public void CheckСertificate()
        {
            courses = mainPage.ClickOnCoursesButton();
            kyivCoursesPage = courses.ClickOnKyivCoursesButton();
            kyivCoursesPage.ClickOnSertificaitButton();

            //проверяем что появился нужный pop up
            string titleGrandPopUp = kyivCoursesPage.GetTextFromSertificatePopUpname();
            Assert.AreEqual("Онлайн проверка сертификата", titleGrandPopUp);

            kyivCoursesPage.EnterNumberOfSertificate("PI51NE 561422");
            kyivCoursesPage.EnterTextToNameField("Ishchenko");
            kyivCoursesPage.ClickCheckSertificateButton();

            //проверяем ответ
            string textAnswerForPositiveRespond = kyivCoursesPage.GetTextAnswerForPositiveRespond();
            //искать текст в body странички
            Assert.AreEqual("Сертификат действителен", textAnswerForPositiveRespond);
        }
        //проверка сертификата при заполнении поля имени невалидными данными
        [Test]
        public void CheckCertificateNotValidNameData()
        {
            courses = mainPage.ClickOnCoursesButton();
            kyivCoursesPage = courses.ClickOnKyivCoursesButton();
            kyivCoursesPage.ClickOnSertificaitButton();

            string titleGrandPopUp = kyivCoursesPage.GetTextFromSertificatePopUpname();
            Assert.AreEqual("Онлайн проверка сертификата", titleGrandPopUp);

            kyivCoursesPage.EnterNumberOfSertificate("PI51NE 561420");
            kyivCoursesPage.EnterTextToNameField("i");
            kyivCoursesPage.ClickCheckSertificateButton();

            string textAnswerForPositiveRespond = kyivCoursesPage.GetTextAnswerForNegativeRespond();
            Assert.AreEqual("Что-то пошло не так.", textAnswerForPositiveRespond);
        }
        //проверяем перевод заголовка сайта с русского на английский
        [Test]
        public void CheckLocalizationRussianToEnglish()
        {
            devEducationPageOnENG = mainPage.ClickOnLanguageMenu()
                .LanguageSelectionENG();
            string textEng = devEducationPageOnENG.GetEnglishText();
            Assert.AreEqual("International IT College", textEng);
        }
        //проверяем перевод заголовка сайта с русского на украинский
        [Test]
        public void CheckLocalizationRussianToUkrainian()
        {
            devEducationPageOnUkrainian = mainPage.ClickOnLanguageMenu()
                .LanguageSelectionUKR();
            string textUkr = devEducationPageOnUkrainian.GetUkrainianText();
            Assert.AreEqual("Міжнародний IT-коледж", textUkr);
        }
        //проверяем перевод заголовка сайта с русского на азербайджанский
        [Test]
        public void CheckLocalizationRussianToAzerbaijani()
        {
            devEducationPageOnAzerbaijani = mainPage.ClickOnLanguageMenu()
                .LanguageSelectionAz();
            string textAz = devEducationPageOnAzerbaijani.GetAzerbaijaniText();
            Assert.AreEqual("Beynəlxalq İT kolleci", textAz);
        }
        //Проверка возможности отправки формы при заполнении валидными данными (форма записи на выбранный курс в Киеве)
        [Test]
        public void ValidDataInGetCoursesFormInKyivForJava()
        {
            callPage = mainPage.ClickOnCoursesButton()
                 .ClickOnKyivCoursesButton()
                 .ClickOnJavaCourseButton()
                 .ClickOnSertificaitButton()
                 .EnterTextToNameFieldJavaCourse("test")
                 .EnterTextToPhoneFieldJavaCourse("0975677678")
                 .EnterTextToEmailFieldJavaCourse("ki@email.com")
                 .ClickOnCityDropDownKyiv()
                 .ClickOnCoursesDropDownJava()
                 .ClickOnJavaCoursesKyivButtonForSendForm();

            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);
        }
        //Проверка возможности отправки формы при заполнении не валидными данными (форма записи на выбранный курс в Киеве)
        [Test]
        public void NotValidDataInGetCoursesFormInKyivForJava()
        {
            javaCoursesKyiv = mainPage.ClickOnCoursesButton()
                 .ClickOnKyivCoursesButton()
                 .ClickOnJavaCourseButton()
                 .ClickOnSertificaitButton()
                 .EnterTextToNameFieldJavaCourse("test")
                 .EnterTextToPhoneFieldJavaCourse(" ")
                 .EnterTextToEmailFieldJavaCourse("email.com@")
                 .ClickOnCityDropDownKyiv()
                 .ClickOnCoursesDropDownJava()
                 .ClickOnJavaCoursesKyivButtonForSendFormNotValid();
            string popUpForSignUpKyivJavaCourses = javaCoursesKyiv.GetPopUpNameForJavaCoursesKyiv();
            Assert.AreEqual("Записаться на курс", popUpForSignUpKyivJavaCourses);
        }


        /*
        Проверка отправки формы Присоедениться со страницы блога
        */
        [Test]
        public void CheckJoinFormOnBlogPage()
        {
            blogPage = mainPage.ClickOnBlogsOnTrainingCenterInDropDown();
            callPage = blogPage.ClickOnButtonGoToSecondPageOnBlogPage().
                ClickOnImageGitHubOrGitLabArticle().
                ClickOnButtonJoin().
                EnterToFormInBlogPageName("TestHelloMaxim").
                EnterToFormInBlogPagePhone("(044)495-24-88").
                ClickOnbuttonSend();
            string dontMissCallText = callPage.GetTextFromDontMissCallLabel();
            string descriptionText = callPage.GetTextFromDontDescriptionLabel();
            Assert.AreEqual("Не пропустите наш звонок!", dontMissCallText);
            Assert.AreEqual("В ближайшее время с вами свяжется администратор колледжа.", descriptionText);
        }
        /*
        Проверка отправки формы Подписаться со страницы Блог
        */
        [Test]
        public void CheckSubscribeFormOnBlogPage()
        {
            blogPage = mainPage.ClickOnBlogsOnTrainingCenterInDropDown();
            captcha = blogPage.EnterToFormClosedItLinkFormEmail("TestPrivetMaxim@gmail.com").
                ClickOnButtonSubscribe();
            string titleFromCaptcha = captcha.GetTextFromTitleOnTheCaptchaPage();
            Assert.AreEqual("Чтобы получать письма, подтвердите, что вы – человек.", titleFromCaptcha);
        }

        [OneTimeTearDown]
        public void KillDriver()
        {
            driver.Quit();
        }

    }
}

